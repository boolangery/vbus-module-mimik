import React from "react";
import withMyAuth from "../withMyAuth";
import Layout from "../components/Layout";
import {useHistory} from "react-router-dom";


function DonePage() {
    const history = useHistory()
    return (
        <Layout>
            Done
            <button className="btn btn-primary" style={{}} onClick={() => history.goBack()}>
                Go back
            </button>
        </Layout>
    )
}


export default withMyAuth(DonePage)
