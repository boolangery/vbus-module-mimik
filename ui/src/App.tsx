import React from 'react';
import './App.css';
import HomePage from "./pages/HomePage";
import {VbusAccessContext, VbusRoute} from "@veea/react-vbus-access"
import {BrowserRouter, Route, Link} from "react-router-dom";
import DonePage from "./pages/DonePage";


const DEV = (!process.env.NODE_ENV || process.env.NODE_ENV === 'development');



export default function App() {
    const baseUrl = DEV ? "http://localhost:8080/api/v1/" : `${window.location.origin}/api/v1/`

    return (
        <div className="App">
            <BrowserRouter>
                <Link to="/done">menu</Link>
                <VbusAccessContext baseUrl={baseUrl}>
                    <VbusRoute path="/done" exact>
                        <DonePage/>
                    </VbusRoute>

                    <VbusRoute path="/" exact>
                        <HomePage/>
                    </VbusRoute>

                    <Route path="/api/v1/static/system/mimik/boolangery-ThinkPad-P1-Gen-2/index.html" exact>
                        <HomePage/>
                    </Route>
                </VbusAccessContext>
            </BrowserRouter>
        </div>
    );
}
