# react-vbus-access

A vbus-access wrapper for React.

## usage

The recommended usage is to use React hooks.

## context

Before starting to use this library, you must wrap your whole application inside an `ApiContext`:

```typescript jsx
import React from 'react';
import './App.css';
import {LocalStorage, Client} from "@veea/vbus-access";
import {ApiContext} from "@veea/react-vbus-access"


const client = new Client({
    baseUrl: "http://localhost:8080/api/v1/",
    storage: new LocalStorage(),
    onUnauthenticated: (api) => {
        console.log('onUnauthenticated');
        return api.login.post({
            returnUrl: window.location.href
        }).then(r => {
            window.location.replace(r.data.viewUrl)
        }).catch(r => {
            console.error(r)
        })
    }
});

export default function App() {
    return (
        <div className="App">
            <ApiContext.Provider value={client}>
                <Home/>
            </ApiContext.Provider>
        </div>
    );
}
```

## get modules

```typescript jsx
import React from "react";
import {useModules} from "@veea/react-vbus-access"


export default function HomePage() {
    const {modules, error} = useModules();

    if (!modules) {
        return (<div>loading...</div>)
    }

    if (error) {
        return (<label>Error: {error.message}</label>)
    } 

    return (
        <ul>
            {
                modules.map(m => <li>{m.id}</li>)
            }
        </ul>
    )
}

```
