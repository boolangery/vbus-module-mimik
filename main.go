package main

import (
	"bitbucket.org/vbus/vbus.go"
	"bitbucket.org/veeafr/utils.go/logging"
)

var _logger = logging.GetNamedLogger()

func main() {
	client := vBus.NewClient("system", "mimik", vBus.WithStaticPath("./ui/build"))
	if err := client.Connect(); err != nil {
		_logger.Fatal(err)
	}
	defer func() {
		if err := client.Close(); err != nil {
			_logger.Error(err)
		}
	}()

	var tokenStorage = ""

	setCredentials := func(token string, parts []string) {
		tokenStorage = token
	}

	_, err := client.AddNode("config", vBus.RawNode{
		"setCredentials": vBus.NewMethodDef(setCredentials),
	})
	if err != nil {
		_logger.Error(err)
	}

	select{}
}
