import React, { useContext } from "react";
import withMyAuth from "../withMyAuth";
import Layout from "../components/Layout";
import { useHistory, useLocation } from "react-router-dom";
import {ApiContext} from "@veea/react-vbus-access";

const mimikLoginUrl = "https://mid.mimik360.com/auth?scope=openid+email&response_type=code&client_id=030573e7-9ab2-4d52-ae3b-b745697652f5&code_challenge=czD7gtNh2SowYqxpN5OSf5a6wIiszEZ9AvRHGvwIJS4&code_challenge_method=S256&redirect_uri=http://dashboard.local&edge_id_token=eyJhbGciOiJSUzI1NiIsInR5cCI6IkpXVCIsImtpZCI6IjdpSWZQUHFmMW0zQXNiMWQzS2tjQ3piYkwxd1pGc3pIQ2x6cHRNbEdBaWMifQ.eyJlbWFpbCI6Im1ldGluLmJhcnV0QG1pbWlrLmNvbSIsInN1YiI6IjI0MzI2NTE0NTM4MDk0NTEwMDgiLCJhdF9oYXNoIjoiOVl0YlVWU3I2M1ZiYVpUWk1PWEJMdyIsInNpZCI6ImFlZjQ0YWZjLTc1OGEtNDA1Mi05ZWVlLTMyMDVhZWFmYjg2MiIsImF1ZCI6IjFkMTYzM2IzLWEzZTctNDAxMi05MDE3LWVkZWIyZGM2ZTliOSIsImV4cCI6MTU4NTM1MTY3OCwiaWF0IjoxNTg1MzQ4MDc4LCJpc3MiOiJodHRwczovL21pZC1kZXYubWltaWtkZXYuY29tIn0.Fp4mAdDNuz9F8kBZZm-C6HYJ1mpjAG-vGHujKYhUxjI4J_NpgoE4M7WqV_jUuccqQEA41QJnlPIxL4qfrizVoQu4VOpKSR5RZIyX_0G5cXBFvxAf_2AEKhTe2gGKoPLPcaE-hYqptI6R3zC7yvCXjUxZSjT2KSJr7cfxMk03LRwQ4N-bs3Tjh2sO5z3DA5L7B_nXZAOCVqnUBpYR-6-AVPaqTLcu-Df5QDHWTIXHjCLexIa5bghn-9D96oMtNZpoZXlbsAbZiuc07eChBEqySWLvIbrsBcOiA2OzrIhbR89m36VhGfI6a_sGHg9namxUIc_76mq6JfwZrT8TmmWVWg&state=123xya"


function HomePage() {
    const location = useLocation()
    const history = useHistory()

    const client = useContext(ApiContext)
    if (client === null) {
        throw new Error("useAttribute should be used inside an ApiContext")
    }

    const onClick = () => {
        history.push("/done")
        //window.location.href = mimikLoginUrl
        //window.location.href = "http://localhost:8080/api/v1/static/system/mimik/boolangery-ThinkPad-P1-Gen-2?foo=bar"
    }

    return (
        <Layout>
            <button className="btn btn-primary" style={{}} onClick={onClick}>
                Go to Mimik auth page
            </button>
        </Layout>
    )
}


export default withMyAuth(HomePage)
