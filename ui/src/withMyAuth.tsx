import React from "react";
import {Redirect} from "react-router-dom";
import {withAuthentication} from "@veea/react-vbus-access"


/**
 * Just a wrapper to preset fallback and loading param.
 */
const withMyAuth = (component: React.ComponentType) => withAuthentication(component, <Redirect to="/login"/>, <div/>)

export default withMyAuth
